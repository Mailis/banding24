import SquareSlotColor from './square_slot_color';

abstract class SquareSlotColorBehaviour {
  availablecolor:SquareSlotColor;
  unvailablecolor:SquareSlotColor;
  partiallyAvailablecolor:SquareSlotColor;
  constructor(){
    this.availablecolor = SquareSlotColor.available;
    this.unvailablecolor = SquareSlotColor.unavailable;
    this.partiallyAvailablecolor = SquareSlotColor.partially_available;
  }
  abstract getColor(): string;
}
  
export default SquareSlotColorBehaviour;