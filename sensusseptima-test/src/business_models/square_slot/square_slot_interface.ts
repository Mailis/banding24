import SquareSlotType from './square_slot_type';

interface SquareSlotInterface {
  tooltipMessage: string | undefined;
  stateMessage: string | undefined;
  stype: SquareSlotType;
  color: string;
}

export default SquareSlotInterface;