import Project from '../business_models/project/project';
import SquareSlotType from '../business_models/square_slot/square_slot_type';
import RandomSlotGenerator from './RandomSlotGenerator';

class ProjectGenerator{

    public static generateProjectsList():Project[]{
        let pList :Project[] = [];
        ['A', 'B', 'C', 'D'].forEach((l, index) =>{
            pList.push(this.makeProject(l, index));
        })
        return pList;
    }

    private static makeProject(letter : string, index:number) : Project{
        let lastSlotType =  SquareSlotType.available
        if(index === 1){
            lastSlotType =  SquareSlotType.partially_available;
        }
        else if(index === 3){
            lastSlotType =  SquareSlotType.unavailable;
        }
        let p:Project = new Project("Project " + letter);
        p.setSlotList (RandomSlotGenerator.getListOfSlots(lastSlotType));
        p.setLastSlotType(lastSlotType);
        p.calculateAvailabilityPercent();
        return p;
    }

}

export default ProjectGenerator;