enum SquareSlotStateMessage {
    available = "",
    unavailable = "Unavailable",
    partially_available = "Partially Unavailable"
}
export default SquareSlotStateMessage;