import React from 'react';
import SquareSlotType from '../business_models/square_slot/square_slot_type';
import formatDate from '../utils/TimeFormatter';

function Tooltip(props:any) {
    const icon = props.stype === SquareSlotType.unavailable? 
        <span className="material-icons" style={props.tootltipIconColor}>
        error
        </span> :
        <span className="material-icons" style={props.tootltipIconColor}>
        warning
        </span>;
   
    const timeHeader = formatDate(props.clockTime);

    return (
    <div className="tooltip">
        <div className="tooltip_title">
            {timeHeader}
        </div>
        <div className="tooltip_message">
            <div className="tooltip_icon">
                {icon}
            </div>
            <div className="tooltip_text">
                {props.message}
            </div>
            <div className="tooltip_text">
                {props.minutes + " min"}
            </div>
            <div className="tooltip_text">
                {props.seconds + " sec"}
            </div>
        </div>
        
    </div>
    );
}

export default Tooltip;