enum SquareSlotType {
    available = "GREEN",
    unavailable = "RED",
    partially_available = "ORANGE"
}
export default SquareSlotType;