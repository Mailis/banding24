import SquareSlot from '../square_slot/square_slot';
import SquareSlotType from '../square_slot/square_slot_type';
import nrOfSlots from '../../data/NrOfSlots';

class Project{
    name:string;
    availability: number = 0;
    slotList:SquareSlot[] = [];
    lastSlotType:SquareSlotType = SquareSlotType.available;

    constructor(name: string) {
      this.name = name;
    }

    public setSlotList(sList:SquareSlot[] ){
        this.slotList = sList;
    }

    public setLastSlotType(lastSlotType:SquareSlotType){
        this.lastSlotType = lastSlotType;
    }

    public calculateAvailabilityPercent(){
        let counter_availabilty: number = 0;
        this.slotList.forEach(item => {
            if(item.stype === SquareSlotType.available){
                counter_availabilty++;
            }
        })

        this.availability = (counter_availabilty * 100) / nrOfSlots;
    }
 
}

export default Project;
