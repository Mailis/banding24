function formatDate(timestamp:number):string{
    const date = new Date(timestamp);
    function pad(number:number) {
        if (number < 10) {
          return '0' + number;
        }
        return number;
     }

     function getZone(mins:number) {
        const hours_tmp:number = mins/60;
        const remainingMins:number = mins - hours_tmp*60;
        const hours:string = hours_tmp > 0 ? "+" + hours_tmp : hours_tmp.toString();
        return hours + ":" + pad(remainingMins);
     }
  
     let formatted:string = 
        pad(date.getUTCHours()) +
        ':' + pad(date.getUTCMinutes()) +
        ':' + pad(date.getUTCSeconds()) +
        ' [' + getZone(-date.getTimezoneOffset()) +
        '] ' +
        ' / ' + pad(date.getUTCDate()) +
        '/' + pad(date.getUTCMonth() + 1) +
        '/' + date.getUTCFullYear() ;
    return formatted;
}

export default formatDate;