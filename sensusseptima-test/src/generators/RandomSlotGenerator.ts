import SquareSlot from '../business_models/square_slot/square_slot';
import SquareSlotType from '../business_models/square_slot/square_slot_type';
import SquareSlotFactory from '../business_models/square_slot/square_slot_factory';
import nrOfSlots from '../data/NrOfSlots';
import SlotTimeCalculator from '../calculations/slot_time_calculator';
import RandomSlotTypeGenerator from './RandomSlotTypeGenerator';
import YestardyTimeGenerator from './YestardyTimeGenerator';

class RandomSlotGenerator{

    public static getListOfSlots(lastSlotType:SquareSlotType) : SquareSlot[]{
        const yesterdayTimeStamp:number = YestardyTimeGenerator.getTime24HoursAgo();
        let slotList:SquareSlot[] = [];

        for(let i = 0; i < nrOfSlots; i++){
            let randomType:SquareSlotType = RandomSlotTypeGenerator.generateRandomSlotType(i, lastSlotType);
            const currentSlotTime = SlotTimeCalculator.calculateSoltTime(i+1,yesterdayTimeStamp);
            let s: SquareSlot = SquareSlotFactory.getSlot(randomType, currentSlotTime);
            slotList.push(s);
        }
        return slotList;
    }
}

export default RandomSlotGenerator;