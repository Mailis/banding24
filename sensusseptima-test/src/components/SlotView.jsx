import React from 'react';
import Tooltip from './Tooltip';

class SlotView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {showTooltip: false};
  }

  displayTooltip = (e) =>{
    if(e)
      e.preventDefault();
    this.setState({showTooltip : true})
  }
  hideTooltip = (e) =>{
    if(e)
      e.preventDefault();
    this.setState({showTooltip : false})
  }

  render() {
    const style = {
      backgroundColor : this.props.color
    }
    const tootltipIconColor = {
      color : this.props.color
    }
    const message = this.props.tooltipMessage;
    const stype = this.props.stype;
    const showTooltip = this.state.showTooltip;
    const slotTime = this.props.slotTime;

    const clockTime = slotTime && new Date(slotTime[2]);
    const minutes = slotTime && slotTime[0];
    const seconds = slotTime && slotTime[1];
    
    return (
      <div className="slot" style={style}
        onMouseOver = {this.displayTooltip}
        onMouseOut = {this.hideTooltip}>
          {showTooltip && message !== "" &&
          <Tooltip className="hei"
            clockTime = {clockTime}
            minutes={minutes}
            seconds={seconds}
            message = {message}
            tootltipIconColor={tootltipIconColor}
            stype={stype}>
          </Tooltip>}
      </div>
    );
  }
    
}

export default SlotView;