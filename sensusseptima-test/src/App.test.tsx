import React from 'react';
import renderer from 'react-test-renderer';
import { render } from '@testing-library/react';
import App from './App';
import SlotView from './components/SlotView';
import formatDate from './utils/TimeFormatter';
import SquareSlotType from './business_models/square_slot/square_slot_type';
import Tooltip from './components/Tooltip';

test('renders: Uptime over the past 24 hours', () => {
  const { getByText } = render(<App />);
  const textElement = getByText(/Uptime over the past 24 hours/i);
  expect(textElement).toBeInTheDocument();
});


test('renders: 24 hours ago', () => {
  const { getAllByText } = render(<App />);
  const textElement = getAllByText(/24 hours ago/i);
  expect(textElement).toBeInstanceOf(Array);
});

test('renders: Project', () => {
  const { getAllByText } = render(<App />);
  const textElement = getAllByText(/Project/i);
  expect(textElement).toBeInstanceOf(Array);
});


test('renders: % availability', () => {
  const { getAllByText } = render(<App />);
  const textElement = getAllByText(/% availability/i);
  expect(textElement).toBeInstanceOf(Array);
});

test('renders: Today', () => {
  const { getAllByText } = render(<App />);
  const textElement = getAllByText(/Today/i);
  expect(textElement).toBeInstanceOf(Array);
});


test('SlotView shows tooltip when mouse is over it', () => {
  let testToolTipMessage = "TestTooltipMess";
  let color = "red";
  let type = SquareSlotType.unavailable;
  let now = new Date();
  let currentSlotTime = formatDate(now.getDate());
  const slotview = <SlotView 
      tooltipMessage = {testToolTipMessage}
      color={color}
      stype =  {type}
      slotTime={currentSlotTime}>
    </SlotView>;
  const component = renderer.create(
    slotview,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();

  // manually trigger the callback
  tree.props.onMouseOver();
  // re-rendering
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();

  //tooltip exists?
  const slotViewInstance = component.root.findByType(Tooltip);
  expect(slotViewInstance.props.stype).toBe(type);
  const tooltipInstance = slotViewInstance.findByProps({className: "tooltip"})
  expect(tooltipInstance);
  expect(tooltipInstance.children[0].props.className).toBe("tooltip_title");
  expect(tooltipInstance.children[1].props.className).toBe("tooltip_message");

  // manually trigger the callback
  tree.props.onMouseOut();
  // re-rendering
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});