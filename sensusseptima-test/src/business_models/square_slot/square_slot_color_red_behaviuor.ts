import SquareSlotColorBehaviour from './square_slot_color_behaviuor';
import SquareSlotColor from './square_slot_color';

class SquareSlotColorRedBehaviour extends SquareSlotColorBehaviour{
    getColor(): string {
        return SquareSlotColor.unavailable;
    }
}

export default SquareSlotColorRedBehaviour;