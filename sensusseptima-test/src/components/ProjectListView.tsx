import React from 'react';
import ProjectView from './ProjectView';
import Project from '../business_models/project/project';
import ProjectGenerator from '../generators/ProjectGenerator';


function ProjectListView () {
    
    function displayProjectList():any[]{
        let plist :any[] = [];
        const projectsList = ProjectGenerator.generateProjectsList();
        
        projectsList.forEach((item: Project, index) =>  {
            plist.push(
                    <ProjectView key={item.name + index}
                        name = {item.name}
                        slotList = {item.slotList}
                        availability = {item.availability}
                    >
                    </ProjectView>
            );
        })
        return plist;
    }

    const projectList = displayProjectList();

    return (
        <div className="project_list">
            {projectList}
        </div>
    );
}

export default ProjectListView;