
import nrOfSlots from '../data/NrOfSlots';

class SlotTimeCalculator{
    //calculate how much time a slot has
    private static calculateSoltTimeWindowWidth():number{
        const milliseconds = 24*60*60*1000;
        return  Math.floor(milliseconds/nrOfSlots);
    }

    public static calculateSoltTime(nThSlot:number, startTime:number):[number, number, number]{
        const windowWidth = SlotTimeCalculator.calculateSoltTimeWindowWidth();
        const timeInMilliSeconds = nThSlot*windowWidth;
        const seconds_tmp = Math.floor(windowWidth / 1000);
        const minutes = Math.floor(seconds_tmp/60);
        const seconds = seconds_tmp - minutes*60;
        const currentTime = startTime + timeInMilliSeconds;
        return [minutes, seconds, currentTime];
    }
}
export default SlotTimeCalculator;