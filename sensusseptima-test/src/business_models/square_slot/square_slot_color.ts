enum SquareSlotColor {
    available = "#00ff80",
    unavailable = "red",
    partially_available = "orange",
}
export default SquareSlotColor;