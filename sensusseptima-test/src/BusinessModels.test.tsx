
import SquareSlot from './business_models/square_slot/square_slot';
import SquareSlotType from './business_models/square_slot/square_slot_type';
import SquareSlotColor from './business_models/square_slot/square_slot_color';
import SquareSlotStateMessage from './business_models/square_slot/square_slot_message_state';
import SquareSlotTooltipMessage from './business_models/square_slot/square_slot_message_tooltip';
import SquareSlotFactory from './business_models/square_slot/square_slot_factory';
import nrOfSlots from './data/NrOfSlots';
import SlotTimeCalculator from './calculations/slot_time_calculator';
import YestardyTimeGenerator from './generators/YestardyTimeGenerator';
import Project from './business_models/project/project';
import RandomSlotGenerator from './generators/RandomSlotGenerator';

//test setup
let randomType:SquareSlotType = SquareSlotType.unavailable;
const yesterdayTimeStamp:number = YestardyTimeGenerator.getTime24HoursAgo();
const currentSlotTime = SlotTimeCalculator.calculateSoltTime(2, yesterdayTimeStamp);
let squareSlot: SquareSlot = SquareSlotFactory.getSlot(randomType, currentSlotTime);

//slot creation and poperties
test('creates a slot', () => {
  expect(squareSlot).toBeInstanceOf(SquareSlot);
});
test('slot type', () => {
    expect(squareSlot.stype).toBe(randomType);
});
test('slot time', () => {
    expect(squareSlot.currentSlotTime).toBe(currentSlotTime);
});
test('slot state message', () => {
    expect(squareSlot.stateMessage).toBe(SquareSlotStateMessage.unavailable);
});
test('slot tooltip message', () => {
    expect(squareSlot.stateMessage).toBe(SquareSlotTooltipMessage.unavailable);
});
test('slot color', () => {
    expect(squareSlot.color).toBe(SquareSlotColor.unavailable);
});


//project
test('project properties', () => {
    const name = "Project T"
    let p:Project = new Project(name);
    const lastTypeInList:SquareSlotType = SquareSlotType.partially_available;
    p.setSlotList (RandomSlotGenerator.getListOfSlots(lastTypeInList));
    p.setLastSlotType(lastTypeInList);
    p.calculateAvailabilityPercent();
    expect(p.name).toBe(name);
    expect(p.slotList.length).toBe(nrOfSlots);
    expect(p.lastSlotType).toBe(lastTypeInList);
});