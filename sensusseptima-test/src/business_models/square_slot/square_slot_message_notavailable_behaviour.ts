import SquareSlotMessageBehaviour from './square_slot_message_behaviour';

class SquareSlotMessageNotAvailableBehaviour extends SquareSlotMessageBehaviour{
    getTooltipMessage(): string {
        return this.unvailable_TooltipMessage;
    }
    getProjectStateMessage(): string {
        return this.unvailable_StateMessage;
    }
}

export default SquareSlotMessageNotAvailableBehaviour;