import SquareSlotTooltipMessage from './square_slot_message_tooltip';
import SquareSlotStateMessage from './square_slot_message_state';

abstract class SquareSlotMessageBehaviour {

    available_TooltipMessage:SquareSlotTooltipMessage;
    unvailable_TooltipMessage:SquareSlotTooltipMessage;
    partiallyAvailable_TooltipMessage:SquareSlotTooltipMessage;
    available_StateMessage:SquareSlotStateMessage;
    unvailable_StateMessage:SquareSlotStateMessage;
    partiallyAvailable_StateMessage:SquareSlotStateMessage;

    public constructor(){
        this.available_TooltipMessage = SquareSlotTooltipMessage.available;
        this.unvailable_TooltipMessage = SquareSlotTooltipMessage.unavailable;
        this.partiallyAvailable_TooltipMessage = SquareSlotTooltipMessage.partially_available;
        this.available_StateMessage = SquareSlotStateMessage.available;
        this.unvailable_StateMessage = SquareSlotStateMessage.unavailable;
        this.partiallyAvailable_StateMessage = SquareSlotStateMessage.partially_available;
    }
    getProjectStateMessage(): string{
        return "";
    };
    
    getTooltipMessage(): string {
        return "";
    }
}
  
export default SquareSlotMessageBehaviour;