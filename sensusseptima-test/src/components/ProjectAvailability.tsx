import React from 'react';

function ProjectAvailability (props:any) {

    return (
        <div className="proj_availability">
            <span className="left"> 24 hours ago</span>
            <div className="line"><span>{props.availability}</span></div>
            <span className="right">Today</span>
        </div>
    );
}

export default ProjectAvailability;