import React from 'react';
import SquareSlot from '../business_models/square_slot/square_slot';
import SlotView from './SlotView';
import ProjectAvailability from './ProjectAvailability';
import ProjectHeaders from './ProjectHeaders';

function ProjectView (props:any) {
    const name:string  = props.name;
    const slotList:SquareSlot[] = props.slotList;
    const availability:string = Math.round(props.availability * 100) / 100 + " % availability";
    const lastIndex = slotList.length-1;
    const lastSlot = slotList[lastIndex];
    const stateMesage:string|undefined = lastSlot.stateMessage;
    const style = {
        color : lastSlot.color
      }
    
    function displaySlotList():any[]{
        let slist: any[] = [];
        slotList.forEach((item:SquareSlot, index) =>  {
            slist.push(
                    <SlotView key={"slot" + index}
                        tooltipMessage = {item.tooltipMessage}
                        color={item.color}
                        stype =  {item.stype}
                        slotTime={item.currentSlotTime}
                        >
                    </SlotView>
            );
        })
        return slist;
    }

    const slotsList = displaySlotList();

    return (
        <div className="project_view_container" >
            <div className="project_view" >
                
                <ProjectHeaders
                    name = {name}
                    stateMesage ={stateMesage}
                    style = {style}>
                </ProjectHeaders>
                <div className="slot_list" >
                    {slotsList}
                </div>
                <ProjectAvailability
                    availability = {availability}>
                </ProjectAvailability>
            </div>
        </div>
    );
}

export default ProjectView;