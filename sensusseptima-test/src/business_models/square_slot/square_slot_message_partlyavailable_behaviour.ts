import SquareSlotMessageBehaviour from './square_slot_message_behaviour';

class SquareSlotMessagePartlyAvailableBehaviour extends SquareSlotMessageBehaviour{
    getTooltipMessage(): string {
        return this.partiallyAvailable_TooltipMessage;
    }

    getProjectStateMessage(): string {
        return this.partiallyAvailable_StateMessage;
    }
}

export default SquareSlotMessagePartlyAvailableBehaviour;