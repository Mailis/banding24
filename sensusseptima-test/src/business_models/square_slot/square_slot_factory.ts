import SquareSlotType from './square_slot_type';
import SquareSlot from './square_slot';
import SquareSlotColorBehaviour from './square_slot_color_behaviuor';
import SquareSlotColorGreenBehaviour from './square_slot_color_green_behaviuor';
import SquareSlotColorOrangeBehaviour from './square_slot_color_orange_behaviuor';
import SquareSlotColorRedBehaviour from './square_slot_color_red_behaviuor';
import SquareSlotMessageBehaviour from './square_slot_message_behaviour';
import SquareSlotMessageNotAvailableBehaviour from './square_slot_message_notavailable_behaviour';
import SquareSlotMessagePartiallyAvailableBehaviour from './square_slot_message_partlyavailable_behaviour';
import SquareSlotMessageAvailableBehaviour from './square_slot_message_available_behaviour';

class SquareSlotFactory{
    public static getSlot(stype:SquareSlotType, currentSlotTime:[number, number, number]):SquareSlot{
        const greenColorBehaviour:SquareSlotColorBehaviour = new SquareSlotColorGreenBehaviour();
        const orangeColorBehaviour:SquareSlotColorBehaviour = new SquareSlotColorOrangeBehaviour();
        const redColorBehaviour:SquareSlotColorBehaviour = new SquareSlotColorRedBehaviour();
        const availableMessageBehaiour:SquareSlotMessageBehaviour = new SquareSlotMessageAvailableBehaviour();
        const notAvailableMessageBehaiour:SquareSlotMessageBehaviour = new SquareSlotMessageNotAvailableBehaviour();
        const partiallyAvailableMessageBehaiour:SquareSlotMessageBehaviour = new SquareSlotMessagePartiallyAvailableBehaviour();
    
        let squareSlot :SquareSlot = new SquareSlot(greenColorBehaviour, availableMessageBehaiour, stype, currentSlotTime);
        if(stype === SquareSlotType.partially_available ){
            squareSlot = new SquareSlot(orangeColorBehaviour, partiallyAvailableMessageBehaiour, stype, currentSlotTime);
        }
        else if(stype === SquareSlotType.unavailable){
            squareSlot = new SquareSlot(redColorBehaviour, notAvailableMessageBehaiour, stype, currentSlotTime);
        }

        return squareSlot;
    }
}

export default SquareSlotFactory;