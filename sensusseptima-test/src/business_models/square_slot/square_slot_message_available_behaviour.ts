import SquareSlotMessageBehaviour from './square_slot_message_behaviour';

class SquareSlotMessageAvailableBehaviour extends SquareSlotMessageBehaviour{
    getTooltipMessage(): string {
        return this.available_TooltipMessage;
    }

    getProjectStateMessage(): string {
        return this.available_StateMessage;
    }
}

export default SquareSlotMessageAvailableBehaviour;