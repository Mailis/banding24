import React from 'react';

function ProjectHeaders (props:any) {

    return (
        <div className="banding_headers">
            <div className="project_name">
                {props.name}
            </div>
            <div className="project_name" style = {props.style}>
                {props.stateMesage}
            </div>
        </div>
    );
}

export default ProjectHeaders;