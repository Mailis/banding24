import SquareSlotType from '../business_models/square_slot/square_slot_type';
import nrOfSlots from '../data/NrOfSlots';
import getRandomIntInclusive from '../utils/GetRandomInt';

class RandomSlotTypeGenerator{
 
    public static generateRandomSlotType(iThSlot:number, lastSlotType:SquareSlotType){
        let randomType:SquareSlotType = SquareSlotType.available;
        if(iThSlot === nrOfSlots-1){
            randomType = lastSlotType;
        }
        else{
            const randomSubtractor_array= [7, 9, 11, 6, 17];
            const randomSubtractor_int = getRandomIntInclusive(0,randomSubtractor_array.length-1);
            const randomSubtractor = randomSubtractor_array[randomSubtractor_int];
            if(iThSlot%randomSubtractor === 0){
                //get random type for slot
                let randomInt = getRandomIntInclusive(1,3);
                if(randomInt === 1){
                    randomType = SquareSlotType.available;
                }
                else{
                    randomType = (randomInt === 2)? SquareSlotType.partially_available : SquareSlotType.unavailable;
                }
            }
        }
        return randomType;
    }
}

export default RandomSlotTypeGenerator;
