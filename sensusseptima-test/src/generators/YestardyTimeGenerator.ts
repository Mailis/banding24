class YestardyTimeGenerator{
    public static getTime24HoursAgo():number{
        const currentTime = new Date(Date.now());
        const yesterday = new Date(currentTime.getTime() - 24*60*60*1000);
        return yesterday.getTime();
    }

}

export default YestardyTimeGenerator;