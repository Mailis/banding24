enum SquareSlotTooltipMessage {
    available = "",
    unavailable = "Unavailable",
    partially_available = "Partial Outage"
}
export default SquareSlotTooltipMessage;