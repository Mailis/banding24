import SquareSlotColorBehaviour from './square_slot_color_behaviuor';

class SquareSlotColorOrangeBehaviour extends SquareSlotColorBehaviour{
    getColor(): string {
        return this.partiallyAvailablecolor;
    }
}

export default SquareSlotColorOrangeBehaviour;