import SquareSlotColorBehaviour from './square_slot_color_behaviuor';

class SquareSlotColorGreenBehaviour extends SquareSlotColorBehaviour{
    getColor(): string {
        return this.availablecolor;
    }
}

export default SquareSlotColorGreenBehaviour;