import React from 'react';
import './App.css';
import ProjectListView from './components/ProjectListView';

function App() {
  return (
    <div className="App">
      <div className="container" >
        <div className="list_title"> Uptime over the past 24 hours</div>
        <div className="clear"></div>
        <ProjectListView></ProjectListView>
      </div>
    </div>
  );
}

export default App;