import SquareSlotInterface from './square_slot_interface';
import SquareSlotColorBehaviour from './square_slot_color_behaviuor';
import SquareSlotMessageBehaviour from './square_slot_message_behaviour';
import SquareSlotType from './square_slot_type';

class SquareSlot implements SquareSlotInterface{
  tooltipMessage: string | undefined;
  stateMessage: string | undefined;
  color: string;
  stype: SquareSlotType;
  currentSlotTime: [number, number, number];
    
    
    constructor(colorBehaviur: SquareSlotColorBehaviour, messageBehaviur: SquareSlotMessageBehaviour, 
      stype:SquareSlotType, currentSlotTime: [number, number, number]) {
      this.color = colorBehaviur.getColor();
      this.tooltipMessage = messageBehaviur.getTooltipMessage();
      this.stateMessage = messageBehaviur.getProjectStateMessage();
      this.stype = stype;
      this.currentSlotTime = currentSlotTime;
    }
}

export default SquareSlot;